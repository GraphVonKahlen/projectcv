import {Component, OnInit} from '@angular/core';
import {LoginServices} from '../../services/login.services';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss']
})
export class MainNavComponent implements OnInit {
  title = 'bgeCvTeck';

  constructor() {

  }

  ngOnInit() {

  }
  loginstatus() {
    return LoginServices.getIsLogedIn();
}
}
