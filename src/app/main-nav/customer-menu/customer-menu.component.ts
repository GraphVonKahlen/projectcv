import { Component, OnInit} from '@angular/core';
import {LoginServices} from '../../services/login.services';

@Component({
  selector: 'app-customer-menu',
  templateUrl: './customer-menu.component.html',
  styleUrls: ['./customer-menu.component.scss']
})
export class CustomerMenuComponent implements OnInit {

  constructor() {

  }

  ngOnInit() {
  }
  signOut() {
    LoginServices.signOut();
  }
}
