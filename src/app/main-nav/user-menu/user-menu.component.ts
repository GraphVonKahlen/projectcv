import { Component, OnInit } from '@angular/core';
import {LoginServices} from '../../services/login.services';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent implements OnInit {



  constructor() {

  }

  ngOnInit() {
  }
  signIn() {
    LoginServices.signIn();
  }
}
