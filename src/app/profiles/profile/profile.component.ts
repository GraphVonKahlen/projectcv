import { Component, Input, OnInit } from '@angular/core';
import {ProfileServices} from '../../services/profile.services';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  @Input() profileFirstName: string;
  @Input() profileLastName: string;
  @Input() profileFormation: string;
  @Input() profileStatus: string;
  @Input() profileResume: string;
  @Input() profileImage: string;

  constructor( private profileServices: ProfileServices, private route: ActivatedRoute) { }

  whichJob() {
    switch (this.profileFormation) {
      case 'Développeur web':
        return 'blue';
      case 'Designer':
        return 'yellow';
      case 'Réseau':
        return 'orange';
      case 'Commercial':
        return 'green';
      case 'management':
        return 'red';
    }
  }

  ngOnInit() {
  }

}
