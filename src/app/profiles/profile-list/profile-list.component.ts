import { Component, OnInit } from '@angular/core';
import {ProfileServices} from '../../services/profile.services';


@Component({
  selector: 'app-profile-list',
  templateUrl: './profile-list.component.html',
  styleUrls: ['./profile-list.component.scss']
})
export class ProfileListComponent implements OnInit {
  isLogedin =false;

  profiles = [
    {
      firstName: 'Profile Name',
      lastName: 'Profile Family Name',
      status: 'Profile status',
      formation : 'Développeur web',
      resume: 'Profile Resume',
      image: 'assets/images/profile.png',
    },
    {
      firstName: 'Profile Name',
      lastName: 'Profile Family Name',
      status: 'Profile status',
      formation : 'Designer',
      resume: 'Profile Resume',
      image: 'assets/images/profile.png',
    },
    {
      firstName: 'Profile Name',
      lastName: 'Profile Family Name',
      status: 'Profile status',
      formation : 'Réseau',
      resume: 'Profile Resume',
      image: 'assets/images/profile.png',
    },
  ];
  constructor( private profileServices: ProfileServices) { }

  ngOnInit() {
  }

}
