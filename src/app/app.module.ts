import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent} from './app.component';
import { ProfileListComponent } from './profiles/profile-list/profile-list.component';
import {AppRoutingModule} from './app-routing.module';
import { ProfileComponent } from './profiles/profile/profile.component';
import { HomeComponent } from './pages/home/home.component';
import { MainNavComponent } from './main-nav/main-nav/main-nav.component';
import { LoginComponent } from './login/login/login.component';
import { FooterComponent } from './main-nav/footer/footer.component';
import { AboutComponent } from './pages/about/about.component';
import { FormationComponent } from './pages/formation/formation.component';
import { ContactComponent } from './pages/contact/contact.component';
import {ProfileServices} from './services/profile.services';
import { UserMenuComponent } from './main-nav/user-menu/user-menu.component';
import { CustomerMenuComponent } from './main-nav/customer-menu/customer-menu.component';
import {LoginServices} from './services/login.services';




@NgModule({
  declarations: [
    AppComponent,
    ProfileListComponent,
    ProfileComponent,
    HomeComponent,
    MainNavComponent,
    LoginComponent,
    FooterComponent,
    AboutComponent,
    FormationComponent,
    ContactComponent,
    UserMenuComponent,
    CustomerMenuComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [
      ProfileServices,
      LoginServices
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
  constructor() {}
}
