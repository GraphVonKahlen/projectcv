import { Component, OnInit} from '@angular/core';
import {LoginServices} from '../../services/login.services';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [LoginServices]
})
export class LoginComponent implements OnInit {



  constructor() {
  }

  ngOnInit() {

  }
  signIn() {
    LoginServices.signIn();
  }

}
