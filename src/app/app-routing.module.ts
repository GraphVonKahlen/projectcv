import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ProfileListComponent} from './profiles/profile-list/profile-list.component';
import {HomeComponent} from './pages/home/home.component';
import {LoginComponent} from './login/login/login.component';
import {ContactComponent} from './pages/contact/contact.component';
import {FormationComponent} from './pages/formation/formation.component';
import {AboutComponent} from './pages/about/about.component';

const routes: Routes = [
  { path: 'profile-list', component: ProfileListComponent },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'formation', component: FormationComponent },
  { path: '', component: HomeComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
