export class LoginServices {
    public static isLogedIn = false;
    constructor() {

    }
    public static getIsLogedIn() {
        return LoginServices.isLogedIn;
    }

    public static signIn() {
        this.isLogedIn = true;
    }
    public static signOut() {
        this.isLogedIn = false;
    }


}
