export class ProfileServices {
    profiles = [
        {
            firstName: 'Profile Name',
            lastName: 'Profile Family Name',
            status: 'Profile status',
            formation : 'Développeur web',
            resume: 'Profile Resume',
            image: 'assets/images/profile.png',
        },
        {
            firstName: 'Profile Name',
            lastName: 'Profile Family Name',
            status: 'Profile status',
            formation : 'Designer',
            resume: 'Profile Resume',
            image: 'assets/images/profile.png',
        },
        {
            firstName: 'Profile Name',
            lastName: 'Profile Family Name',
            status: 'Profile status',
            formation : 'Réseau',
            resume: 'Profile Resume',
            image: 'assets/images/profile.png',
        },
    ];
}
